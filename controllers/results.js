const express = require('express');


function sumar(req, res, next) {
    const n1 = req.params.n1;
    const n2 = req.params.n2;
    res.send(`La suma es: ${parseInt(n1)+parseInt(n2)}`);
}


function multiplicar(req, res, next) {
    const n1 = req.body.n1;
    const n2 = req.body.n2;
    res.send(`La multiplicacion es: ${parseInt(n1)*parseInt(n2)}`);
}

function dividir(req, res, next) {
    const n1 = req.body.n1;
    const n2 = req.body.n2;
    res.send(`La division es: ${parseInt(n1)/parseInt(n2)}`);
}

function potencia(req, res, next) {
    const n1 = req.body.n1;
    const n2 = req.body.n2;
    res.send(`La potencia es: ${parseInt(n1)**parseInt(n2)}`);
}

function restar(req, res, next) {
    const n1 = req.params.n1;
    const n2 = req.params.n2;
    res.send(`La resta es: ${parseInt(n1)-parseInt(n2)}`);
}

module.exports = {sumar,multiplicar,dividir,potencia,restar};